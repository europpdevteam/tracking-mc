package me.europp.tracking;

import java.util.Random;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class BlockPlacement implements Listener {
	public static Tracking plugin;

	public BlockPlacement(Tracking instance) {
		plugin = instance;
	}

	/**
	 * 
	 * @param evt
	 */
	@EventHandler
	public void onPlayerMove(PlayerMoveEvent evt) {
		Player p = evt.getPlayer();
		Location l = p.getLocation();
		World w = l.getWorld();
		l.setY(l.getY() - 1);
		Block b = w.getBlockAt(l);
		Random r = new Random();
		Material m = b.getType();
		int random = 0;
		if (p.isSprinting() == true) {
			;
			random = r.nextInt(3);
		} else {
			random = r.nextInt(10);
		}
		if (random == 0) {
			if (m == Material.GRASS) {
				b.setType(Material.DIRT);
			} else if (m == Material.STONE) {
				b.setType(Material.COBBLESTONE);
			} else if (m == Material.SAND) {
				b.setType(Material.SANDSTONE);
			} else if (m == Material.NETHERRACK) {
				b.setType(Material.NETHER_BRICK);
			}
		}

	}
}
