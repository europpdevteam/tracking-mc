package me.europp.tracking;

import java.io.File;
import java.util.ArrayList;
import java.util.logging.Logger;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Tracking extends JavaPlugin {
	public static Tracking plugin;
	public final Logger logger = Logger.getLogger("Minecraft");

	public int starTrailSchedulerNight, starTrailSchedulerDay;
	private long time;
	public World trailWorld;
	private ArrayList<Block> stars = new ArrayList<Block>();
	public static final Material STAR_MATERIAL = Material.GLOWSTONE;
	public static final Material SKY_MATERIAL = Material.AIR;

	public final BlockPlacement oS = new BlockPlacement(this);

	@Override
	public void onEnable() {
		String pluginFolder = this.getDataFolder().getAbsolutePath();
		(new File(pluginFolder)).mkdirs();

		PluginManager pm = getServer().getPluginManager();
		pm.registerEvents(oS, this);
		PluginDescriptionFile pdfFile = this.getDescription();
		this.logger.info(pdfFile.getName() + " version " + pdfFile.getVersion()
				+ " is enabled.");

		this.getConfig().options().copyDefaults(true);
		this.saveConfig();

		if (this.getConfig().getBoolean("starTrail")) {
			enableStars();
		}
	}

	@Override
	public void onDisable() {
		PluginDescriptionFile pdfFile = this.getDescription();
		this.logger.info(pdfFile.getName() + " is now disabled.");
		
		disableStars();
	}

	public boolean onCommand(CommandSender sender, Command cmd,
			String commandLabel, String[] args) {
		if (cmd.getName().equalsIgnoreCase("startrailon")) {
			enableStars();
			return true;
		} else if (cmd.getName().equalsIgnoreCase("startrailoff")) {
			disableStars();
			return true;
		}
		return false;
	}

	/*
	 * This command enables the star trail.
	 */
	private void enableStars() {
		if (starTrailSchedulerNight == 0) {
			trailWorld = this.getServer().getWorld(
					this.getConfig().getString("worldName"));
			time = trailWorld.getTime();
			time = (long) ((11.5 * 60 * 20) - time); // 0 ticks= start day;
														// 10 minutes of
														// day, 1,5 minutes
														// of dusk * 60
														// seconds * 20
														// ticks/second -
														// time passed =
														// time to start of
														// night
			starTrailSchedulerNight = getServer().getScheduler()
					.scheduleAsyncRepeatingTask(this, new Runnable() {
						public void run() {
							placeStars();
						}
					}, time, 20L * 60 * 20);// run every night
			starTrailSchedulerDay = getServer().getScheduler()
					.scheduleAsyncRepeatingTask(this, new Runnable() {
						public void run() {
							destroyStars();
						}
					}, time + 20L * 60 * 7, 20L * 60 * 20);// runs at dawn
			getServer().broadcastMessage(ChatColor.GREEN + "Trails are on!");
		} else {
			getServer().broadcastMessage(
					ChatColor.RED + "Trails are already on!");
		}
	}

	/*
	 * Disables the stars trail again.
	 */
	public void disableStars() {
		if(starTrailSchedulerNight != 0){
		getServer().getScheduler().cancelTask(starTrailSchedulerNight);
		getServer().getScheduler().cancelTask(starTrailSchedulerDay);}
		destroyStars();
	}

	/*
	 * This command places the stars at the beginning of the night at the sky
	 * (128 blocks above your head).
	 */
	public void placeStars() {
		getServer().broadcastMessage(ChatColor.GOLD + "Stars Placed!");
		Location loc;
		Block b;
		for (Player player : trailWorld.getPlayers()) {
			loc = player.getLocation();
			loc.setY(loc.getY() + 128);
			b = trailWorld.getBlockAt(loc);
			b.setType(STAR_MATERIAL);
			stars.add(b);
		}
	}

	/*
	 * This command destroys the stars at the end of the night.
	 */
	public void destroyStars() {
		for (Block b : stars) {
			b.setType(SKY_MATERIAL);
		}
		stars.removeAll(stars);
	}

}
